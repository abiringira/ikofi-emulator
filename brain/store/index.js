import React from "react";
import useGlobalHook from "./globalHook";

import * as actions from "../actions";

const initialState = { isUnified: false };

const useGlobal = useGlobalHook(React, initialState, actions);

export default useGlobal;
