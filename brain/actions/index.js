export const setMode = (store, mode) => {
  store.setState({ ...store.state, isUnified: mode == "UNIFIED" });
};

export const setPhone = (store, phone) => {
  store.setState({ ...store.state, phone });
};
