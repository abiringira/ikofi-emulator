import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: "auto"
    // backgroundColor: theme.palette.background.paper
  },
  menu: {
    fontSize: "1px",
    fontWeight: "normal"
  }
}));

export default function ListDividers(props) {
  const classes = useStyles();
  const choices = props.choices && props.choices.length ? props.choices : [];

  const menus = choices.map((choice, index) => {
    return (
      <div key={index}>
        <ListItem
          button
          onClick={() => {
            props.menuClicked(choice.id);
          }}
        >
          <ListItemText
            key={index}
            className={classes.menu}
            primary={choice.id + ")" + choice.text}
          />
        </ListItem>
        <Divider className={classes.root} />
      </div>
    );
  });

  return (
    <List component="nav" className={classes.root} aria-label="Ikofi choices">
      {menus}
    </List>
  );
}
