import Typography from "@material-ui/core/Typography";

export default function Title(props) {
  const { text, size = "sm", isCentered = false } = props;

  const variantSize =
    size == "lg" ? "h2" : size == "md" ? "h3" : size == "xs" ? "h6" : "h5";

  return (
    <Typography variant={variantSize} align={isCentered ? "center" : "left"}>
      {text}
    </Typography>
  );
}
