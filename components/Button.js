import { makeStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";

const useStyles = makeStyles(theme => ({
  margin: { margin: theme.spacing(1) }
}));

export default function Button(props) {
  const classes = useStyles();

  const {
    onClicked = () => alert("No link attached"),
    isColored = true
  } = props;

  return (
    <Fab
      variant="extended"
      size="small"
      color={isColored ? "secondary" : "default"}
      className={classes.margin}
      onClick={() => onClicked()}
    >
      {props.children}
    </Fab>
  );
}
