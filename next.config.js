const { parsed: localEnv } = require("dotenv").config();
// require("dotenv").config();

const withCss = require("@zeit/next-css");
// This is used to be able to import images right from the code.
const withImages = require("next-optimized-images");

module.exports = withImages(withCss({}));

module.exports = {
  target: "serverless"
};
