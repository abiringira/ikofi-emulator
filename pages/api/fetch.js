const { dialer } = require("../../src/utils/dialer");

export default async (req, res) => {
  // const phone = req.params.phone;
  let { phone, isUnified, userInput } = req.query;
  if (userInput == "undefined") userInput = undefined;
  if (isUnified == "false") isUnified = false;

  try {
    const response = await dialer({ phone, isUnified, userInput });
    // console.log(">>>" + JSON.stringify(userInput, null, 2));
    res.json(response);
  } catch (error) {
    console.log("Error from the back end" + error.message);
  }
};
