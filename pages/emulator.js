import { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
// import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { Typography, Badge } from "@material-ui/core";
import Tableau from "../components/Tableau";
import Grid from "@material-ui/core/Grid";
import { CircularProgress, CardMedia, Container } from "@material-ui/core";
import GoBack from "@material-ui/icons/ReplaySharp";
import SwipeTab from "../components/SwipeTab";
import ClickableMenus from "../components/ClickableMenu";
import Title from "../components/Title";
import useGlobal from "../brain/store";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
const BASE_URL = require("../src/config").baseUrl;

const useStyles = makeStyles(theme => ({
  button: {
    margin: theme.spacing(1),
    "text-align": "center"
  },
  // title: { fontSize: 14, "text-align": "left", textColor: "blue" },
  // pos: { marginBottom: 12 },
  progress: { margin: theme.spacing(2) },
  logo: { width: "25vh", margin: theme.spacing(1) },
  card: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  badgeMargin: { margin: theme.spacing(2) },
  root: {
    flexGrow: 1,
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#eceff1"
  },
  userInput: {
    margin: theme.spacing(5),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
}));

const Emulator = () => {
  const classes = useStyles();

  const [globalState, globalActions] = useGlobal();
  const [input, setInput] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const MAX_TEXT_LENGTH = 126;

  const [menu, setMenu] = useState({
    title: [],
    choices: [],
    action: "FB",
    textSize: 0
  });

  useEffect(() => {
    const { phone, isUnified } = globalState;

    fetchData({ phone, isUnified });
  }, []);

  const sendInput = async value => {
    if (!input && !value) return;
    const customerInput = value ? value : input;
    const { phone, isUnified } = globalState;
    fetchData({ phone, isUnified, userInput: customerInput });
    setInput("");
  };

  const restartSession = async () => {
    const { phone, isUnified } = globalState;

    fetchData({ phone, isUnified });
    setInput("");
  };

  const fetchData = async ({ phone, isUnified, userInput }) => {
    setIsLoading(true);

    const fetchUrl =
      BASE_URL +
      `/api/fetch?phone=${phone}&isUnified=${isUnified}&userInput=${userInput}`;

    const { data } = await axios.get(fetchUrl);

    const { choices, action, title, isTimeout, is500, textSize } = data;

    setIsLoading(false);

    if (isTimeout || is500)
      return setMenu({
        choices: [],
        action: "FB",
        title: [
          "The application seems to not be responding: " +
            (isTimeout ? "Timeout" : "Unknown error")
        ]
      });

    setMenu({ choices, action, title, textSize });
  };

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} md={12}>
        <Card className={classes.card}>
          <Box p={1}>
            <CardMedia
              alt="Ikofi logo"
              src="../static/logo-ikofi.png"
              component="img"
              className={classes.logo}
            />
          </Box>

          <Container className={classes.button}>
            <GoBack onClick={() => restartSession()} />
          </Container>

          <SwipeTab
            headers={["USSD", "LOGS"]}
            history={<p> Coming soon</p>} //{<Tableau headers={["Input", "Menu", "Duration"]} />}
            emulator={
              !isLoading ? (
                <div>
                  <CardContent>
                    <Typography color="textSecondary" gutterBottom>
                      {menu.action == "FC" ? "" : "Your session has ended !"}
                    </Typography>

                    <div className={classes.title}>
                      {menu.title.map((text, index) =>
                        text ? <Title key={index} text={text} /> : <br />
                      )}
                    </div>
                  </CardContent>

                  <ClickableMenus
                    choices={menu.choices}
                    menuClicked={value => {
                      sendInput(value);
                    }}
                  />

                  {menu.action == "FC" && (
                    <div className={classes.userInput}>
                      <Grid container spacing={2}>
                        <Grid item xs={12} md={12}>
                          <TextField
                            id="standard-name"
                            label={"Enter response"}
                            autoFocus={true}
                            autoComplete="off"
                            className={classes.textField}
                            onChange={event => setInput(event.target.value)}
                            margin="normal"
                            onKeyDown={e =>
                              e.key == "Enter" ? sendInput() : ""
                            }
                          />
                        </Grid>
                        <Box m={2} />
                        <Badge
                          variant="dot"
                          invisible={menu.textSize < MAX_TEXT_LENGTH}
                          color="error"
                          className={classes.badgeMargin}
                        >
                          <Typography variant="caption">
                            Number of characters: {menu.textSize}
                          </Typography>
                          <Box p={1} />
                        </Badge>
                      </Grid>
                    </div>
                  )}

                  <Box m={2} />
                </div>
              ) : (
                <Box
                  display="flex"
                  style={{ flexGrow: 1 }}
                  justifyContent="center"
                >
                  <CircularProgress className={classes.progress} />
                </Box>
              )
            }
          />
        </Card>
        <Box p={2}>
          <Typography variant="overline" gutterBottom>
            © 2019 Ikofi squad
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );
};

export default Emulator;
