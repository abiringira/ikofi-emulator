import { useState } from "react";
import Link from "../components/Link";
import Box from "@material-ui/core/Box";
import TextField from "@material-ui/core/TextField";

import { makeStyles } from "@material-ui/core/styles";
import Title from "../components/Title";
import CardMedia from "@material-ui/core/CardMedia";
import { AirplanemodeActive, Accessible, Call } from "@material-ui/icons";
import Button from "../components/Button";

import { Grid, Card } from "@material-ui/core";
import useGlobal from "../brain/store";

const useStyles = makeStyles(theme => ({
  logo: { width: "25vh", margin: theme.spacing(1) },

  root: {
    height: "100vh",
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#eceff1"
  },
  card: {
    marginTop: theme.spacing(10),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    minWidth: "50vh"
  },
  button: {alignItems: "center", width: "auto",display: "flex",
  flexDirection: "column"},
  userInput: {
    width: "auto",
    margin: theme.spacing(5),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
}));

// Test it
const Index = () => {
  const classes = useStyles();
  const [currentPhone, setCurrentPhone] = useState("");

  const [globalState, globalActions] = useGlobal();

  const handleChange = name => event => {
    if (isValidPhone) {
      setCurrentPhone(event.target.value);
      globalActions.setPhone(event.target.value);
    }
  };

  return (
    <Grid container className={classes.root}>
      <Grid item xs={12} md={6}>
        <Card className={classes.card}>
          <CardMedia
            alt="Ikofi Logo"
            src="../static/logo-ikofi.png"
            component="img"
            className={classes.logo}
          />
          <Box m={5} />
          <div>
            <Title
              text="Welcome to IKofi Emulator "
              size="sm"
              isCentered={true}
            />
            <div className={classes.userInput}>
              <Box m={2} />
              <TextField
                id="standard-name"
                label="Enter the phone number"
                autoFocus={true}
                autoComplete="off"
                onChange={handleChange("name")}
                margin="normal"
              />
                
            </div>

            <div className={classes.button}>
          
            <Box m={2} />
           
           
            {isValidPhone(currentPhone) && (
              <Link href={`/emulator`} as="/" underline="none">
                

               
                
                 <Button
                  onClicked={() => {
                    if (!isValidPhone(currentPhone)) return;
                    globalActions.setMode("UNIFIED");
                  }}
                >
                  <Call className={classes.extendedIcon} />
                      Dial
                </Button> 


                <Box m={3} /> 
               </Link>
               
            )}
         </div>

          </div>
        </Card>
      </Grid>
    </Grid>
  );
};

const isValidPhone = phone => /^250\d{9}$/.test("25" + phone);

export default Index;
