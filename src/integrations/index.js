const axios = require("axios");
const config = require("../config");
const https = require('https');

//const USSD_TIMEOUT = 80000; // secs

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

const ussdClient = axios.create({
  baseURL: config.ussdServiceUrl,
  httpsAgent: new https.Agent({  
    rejectUnauthorized: false
  })
});

ussdClient.interceptors.request.use(config => {
  const { url, method } = config;
  console.log({ url, method }, `Requesting data from USSD service `);
  return config;
});

ussdClient.interceptors.response.use(
  response => {
    console.log("api response" + JSON.stringify(response.data, null, 2));
    return response;
  },
  error => {
    console.log("Failed with "+error.message)
    Promise.reject(error)
  }
);

module.exports = ussdClient;
