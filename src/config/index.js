const isProduction = process.env.NODE_ENV == "development";

module.exports = {
  port: 3000,
  isProduction,
  ussdServiceUrl: process.env.USSD_SERVICE_URL,
  baseUrl: isProduction
    ? `http://localhost:3000`
    : `https://ikofi-emulator.vercel.app`
};
