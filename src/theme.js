import { createMuiTheme } from "@material-ui/core/styles";
import { red } from "@material-ui/core/colors";

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: { main: "#232fa8" },
    secondary: { main: "#004ba8" },
    error: { main: red.A400 },
    background: { default: "#fff" }
  },
  typography: { useNextVariants: true, fontFamily: "Roboto" }
});

export default theme;
