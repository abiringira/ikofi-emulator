const { formatUssdResponse } = require("./dialer");

describe("Test maping the code for Unified", () => {
  it("should map correctly a menu without choices", () => {
    const payload = {
      action: "FC",
      myResponse: "Ikaze mu IKOFI##Shyiramo PIN yawe"
    };
    const response = formatUssdResponse({ data: payload }, true);

    expect(response.action).toBe("FC");
    expect(response.choices).toStrictEqual([]);
    expect(response.textSize).toBe(payload.myResponse.length);
    expect(response.title).toBe("Ikaze mu IKOFI  Shyiramo PIN yawe");
  });

  it("should map correctly a menu with choices", () => {
    const payload = {
      action: "FB",
      myResponse: "Choose name ##1) Lionel #2) Vanessa#3) Freddy#4) Victoire"
    };
    const response = formatUssdResponse({ data: payload }, true);

    expect(response.action).toBe("FB");
    expect(response.choices.length).toBe(4);
    expect(response.choices[0]).toStrictEqual({ id: "1", text: " Lionel " });
    expect(response.choices[1]).toStrictEqual({ id: "2", text: " Vanessa" });
    expect(response.choices[2]).toStrictEqual({ id: "3", text: " Freddy" });
    expect(response.choices[3]).toStrictEqual({ id: "4", text: " Victoire" });

    expect(response.title).toBe("Choose name  ");
  });

  it("should map correctly a menu with choices and many lines on menu", () => {
    const payload = {
      action: "FB",
      myResponse:
        "We want your name ##Now choose ##1) Lionel #2) Vanessa#3) Freddy#4) Victoire"
    };
    const response = formatUssdResponse({ data: payload }, true);

    expect(response.action).toBe("FB");
    expect(response.choices.length).toBe(4);
    expect(response.choices[0]).toStrictEqual({ id: "1", text: " Lionel " });
    expect(response.choices[1]).toStrictEqual({ id: "2", text: " Vanessa" });
    expect(response.choices[2]).toStrictEqual({ id: "3", text: " Freddy" });
    expect(response.choices[3]).toStrictEqual({ id: "4", text: " Victoire" });

    expect(response.title).toBe("Choose name  ");
  });
});

/**
{"action": "FB", "choices": [{"id": "1", "text": " Lionel "}, {"id": "2", "text": " Vanessa"}, 
{"id": "3", "text": " Freddy"}, {"id": "4", "text": " Victoire"}], "textSize": 57, 
"title": "Choose name  "}

 */
