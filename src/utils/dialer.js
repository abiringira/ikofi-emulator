const ussdClient = require("../integrations");

const dialer = async ({ isUnified, phone, userInput }) => {
  const URL = getUrl({ isUnified, phone, userInput });

  try {
    const response = await ussdClient.get(URL);

    return formatUssdResponse(response, isUnified);
  } catch (error) {
    if (error.code == "ECONNABORTED") return { isTimeout: true };

    return { is500: true };
  }
};

const getUrl = session => {
  const { isUnified, phone, userInput } = session;

  const {
    shortCode: SHORT_CODE,
    startLvlString: start,
    otherLvlString: end
  } = getConfigCodes(isUnified);

  const baseUrl = `/api/v1/${isUnified ? "proxy" : "query"}`;

  const isFirstTime = userInput ? false : true;

  return (
    baseUrl +
    `?msisdn=${phone}&levelStr=${isFirstTime ? start : end}&userMessage=${
      isFirstTime ? SHORT_CODE : userInput
    }`
  );
};

const getConfigCodes = isUnified => {
  return {
    shortCode: isUnified ? "774*3" : "525*2",
    breakCode: isUnified ? "#" : "\n",
    startLvlString: isUnified ? 0 : 1,
    otherLvlString: isUnified ? 1 : 0
  };
};

const formatUssdResponse = (response, isUnified) => {
  const { breakCode: BREAK_CODE } = getConfigCodes(isUnified);

  const { action = "FC", content } = {
    content: isUnified ? response.data.myResponse : response.data,
    action: isUnified ? response.data.action : response.headers.freeflow
  };

  const texts = content.split(BREAK_CODE);

  let title = texts.shift();

  let choices = [];

  texts.map(text => {
    let x = text.split(")");
    if (x[1]) choices.push({ id: x[0], text: x[1] });
    else title += "#" + x[0];
  });

  title = title.split("#");

  return {
    action,
    title,
    choices,
    textSize: content.length
  };
};

module.exports = { dialer, formatUssdResponse };
